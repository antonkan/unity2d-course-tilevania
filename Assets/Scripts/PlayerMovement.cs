using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float runSpeed = 10f;
    [SerializeField] private float jumpSpeed = 5f;
    [SerializeField] private float climbSpeed = 20f;
    [SerializeField] private Vector2 deathKick = new Vector2(20f, 20f);
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform gunTransform;

    float gravityScaleAtStart;
    bool isTouchingGround;
    bool isTouchingLadder;
    bool isAlive = true;

    Vector2 moveInput;
    Rigidbody2D rigidbodyComp;
    BoxCollider2D feetCollider;
    Animator animatorComp;

    // Start is called before the first frame update
    void Start()
    {
        rigidbodyComp = GetComponent<Rigidbody2D>();
        feetCollider = GetComponent<BoxCollider2D>();
        animatorComp = GetComponent<Animator>();
        gravityScaleAtStart = rigidbodyComp.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive) return;
        isTouchingGround = feetCollider.IsTouchingLayers(LayerMask.GetMask("Ground"));
        isTouchingLadder = rigidbodyComp.IsTouchingLayers(LayerMask.GetMask("Climbing"));
        Run();
        ClimbLadder();
        FlipSprite();
        UpdateAnimator();
        Die();
    }

    void OnMove(InputValue value) 
    {
        if (!isAlive) return;
        moveInput = value.Get<Vector2>();
        Debug.Log(moveInput);
    }

    void OnJump(InputValue value)
    {
        if (!isAlive) return;
        if (value.isPressed && isTouchingGround) {
            rigidbodyComp.velocity += new Vector2(0f, jumpSpeed);
        }
    }

    void OnFire(InputValue value)
    {
        if (!isAlive) return;
        if (value.isPressed && bullet != null) {
            GameObject bulletObj = Instantiate(bullet, gunTransform.position, gunTransform.rotation);
            Bullet bulletScript = bulletObj.GetComponent<Bullet>();
            bulletScript.SetDirection(transform.localScale.x > 0 ? 1 : -1);
        }
    }

    void Run() {
        Vector2 velocity = new Vector2(moveInput.x * runSpeed, rigidbodyComp.velocity.y);
        rigidbodyComp.velocity = velocity;
    }

    void ClimbLadder() {
        if (isTouchingLadder) {
            rigidbodyComp.gravityScale = 0;
            Vector2 velocity = new Vector2(rigidbodyComp.velocity.x, moveInput.y * climbSpeed);
            rigidbodyComp.velocity = velocity;
        } else {
            rigidbodyComp.gravityScale = gravityScaleAtStart;
        }
    }

    void FlipSprite() {
        if (Mathf.Abs(rigidbodyComp.velocity.x) > Mathf.Epsilon) {
            Vector3 scale = transform.localScale;
            transform.localScale = new Vector3(Mathf.Sign(rigidbodyComp.velocity.x), scale.y, scale.z);
        }
    }

    void UpdateAnimator() {
        bool isRunning = Mathf.Abs(rigidbodyComp.velocity.x) > Mathf.Epsilon;
        animatorComp.SetBool("isRunning", isRunning);
        bool isClimbing = isTouchingLadder && Mathf.Abs(rigidbodyComp.velocity.y) > Mathf.Epsilon;
        animatorComp.SetBool("isClimbing", isClimbing);
    }

    void Die() {
        if(rigidbodyComp.IsTouchingLayers(LayerMask.GetMask("Enemies", "Hazards"))) {
            isAlive = false;
            animatorComp.SetTrigger("Dying");
            rigidbodyComp.velocity = deathKick;
        }
    }
}
